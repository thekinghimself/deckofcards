﻿using System;

public class Tutorial        
{
    public static void Main()
    {
        // write your code here
        string[] Spade = { "Spade Suit A", "Spade Suit 2", "Spade Suit 3", "Spade Suit 4", "Spade Suit 5", "Spade Suit 6", "Spade Suit 7", "Spade Suit 8", "Spade Suit 9", "Spade Suit 10", "Spade Suit J", "Spade Suit K", "Spade Suit Q" };
        string[] Heart = { "Heart Suit A", "Heart Suit 2", "Heart Suit 3", "Heart Suit 4", "Heart Suit 5", "Heart Suit 6", "Heart Suit 7", "Heart Suit 8", "Heart Suit 9", "Heart Suit 10", "Heart Suit J", "Heart Suit K", "Heart Suit Q" };
        string[] Club = { "Club Suit A", "Club Suit 2", "Club Suit 3", "Club Suit 4", "Club Suit 5", "Club Suit 6", "Club Suit 7", "Spade Suit 8", "Club Suit 9", "Club Suit 10", "Club Suit J", "Club Suit K", "Club Suit Q" };
        string[] Diamond = { "Diamond Suit A", "Diamond Suit 2", "Diamond Suit 3", "Diamond Suit 4", "Diamond Suit  5", "Diamond Suit  6", "Diamond Suit 7", "Diamond Suit 8", "Diamond Suit 9", "Diamond Suit 10", "Diamond Suit J", "Diamond Suit K", "Diamond Suit Q" };



        /// test code
        Console.WriteLine(Spade[0] + "\n");
        Console.WriteLine(Spade[1] + "\n");
        Console.WriteLine(Spade[2] + "\n");
        Console.WriteLine(Spade[3] + "\n");
        Console.WriteLine(Spade[4] + "\n");
        Console.WriteLine(Spade[5] + "\n");
        Console.WriteLine(Spade[6] + "\n");
        Console.WriteLine(Spade[7] + "\n");
        Console.WriteLine(Spade[8] + "\n");
        Console.WriteLine(Spade[9] + "\n");
        Console.WriteLine(Spade[10] + "\n");
        Console.WriteLine(Spade[11] + "\n");
        Console.WriteLine(Spade[12] + "\n");

        Console.WriteLine(Heart[0] + "\n");
        Console.WriteLine(Heart[1] + "\n");
        Console.WriteLine(Heart[2] + "\n");
        Console.WriteLine(Heart[3] + "\n");
        Console.WriteLine(Heart[4] + "\n");
        Console.WriteLine(Heart[5] + "\n");
        Console.WriteLine(Heart[6] + "\n");
        Console.WriteLine(Heart[7] + "\n");
        Console.WriteLine(Heart[8] + "\n");
        Console.WriteLine(Heart[9] + "\n");
        Console.WriteLine(Heart[10] + "\n");
        Console.WriteLine(Heart[11] + "\n");
        Console.WriteLine(Heart[12] + "\n");


        Console.WriteLine(Diamond[0] + "\n");
        Console.WriteLine(Diamond[1] + "\n");
        Console.WriteLine(Diamond[2] + "\n");
        Console.WriteLine(Diamond[3] + "\n");
        Console.WriteLine(Diamond[4] + "\n");
        Console.WriteLine(Diamond[5] + "\n");
        Console.WriteLine(Diamond[6] + "\n");
        Console.WriteLine(Diamond[7] + "\n");
        Console.WriteLine(Diamond[8] + "\n");
        Console.WriteLine(Diamond[9] + "\n");
        Console.WriteLine(Diamond[10] + "\n");
        Console.WriteLine(Diamond[11] + "\n");
        Console.WriteLine(Diamond[12] + "\n");




        Console.WriteLine(Club[0] + "\n");
        Console.WriteLine(Club[1] + "\n");
        Console.WriteLine(Club[2] + "\n");
        Console.WriteLine(Club[3] + "\n");
        Console.WriteLine(Club[4] + "\n");
        Console.WriteLine(Club[5] + "\n");
        Console.WriteLine(Club[6] + "\n");
        Console.WriteLine(Club[7] + "\n");
        Console.WriteLine(Club[8] + "\n");
        Console.WriteLine(Club[9] + "\n");
        Console.WriteLine(Club[10] + "\n");
        Console.WriteLine(Club[11] + "\n");
        Console.WriteLine(Club[12] + "\n");

        Console.ReadKey();
    }
}